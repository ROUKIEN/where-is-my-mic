const originalLog = console.log
const { messages } = require('./messages.json')

console.log = (obj, ...args) => {
  let applied = args
  if (typeof obj ==='string') {
    const rand = Math.floor(Math.random() * Math.floor(messages.length * 10))
    let append = ''
    if (messages[rand]) {
      append = ` - ${messages[rand]}`
    }
    applied = [ obj + append ]
  }

  originalLog.apply(this, applied)
}

module.exports = console
