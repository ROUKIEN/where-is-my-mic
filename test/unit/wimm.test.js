const console = require('../../wimm.js')

describe('wimm', () => {
  it('may append a message to a console log argument of type string', () => {
    const originalRandom = Math.random
    randomSpy = jest.fn(() => 0.41868)
    Math.random = randomSpy

    const msg = 'Hello world'
    console.log(msg)
    
    // expect(spyLog).toHaveBeenCalledWith('Hello world')

    Math.random = originalRandom
  })
})
