# Where is my mic ?

A small package that may ask you to retrieve its microphone when you use console.log.

_(just sandboxing the way we create nodejs packages... sorry about that)_

## Installation


`npm i --save where-is-my-mic`

Then use it in your code:

```javascript
const console = require('where-is-my-mic')

console.log('Hello world !')
```
